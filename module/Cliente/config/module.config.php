<?php
return [
    'doctrine' => [
        'driver' => [
            'cliente_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\YamlDriver::class,
                'paths' => __DIR__ . '/yaml',
            ],
            'orm_default' => [
                'drivers' => [
                    'Cliente' => 'cliente_driver',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            \Cliente\V1\Rest\Clientes\ClientesResource::class => \Cliente\V1\Rest\Clientes\ClientesResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'cliente.rest.clientes' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/clientes[/:id]',
                    'defaults' => [
                        'controller' => 'Cliente\\V1\\Rest\\Clientes\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'cliente.rest.clientes',
        ],
    ],
    'zf-rest' => [
        'Cliente\\V1\\Rest\\Clientes\\Controller' => [
            'listener' => \Cliente\V1\Rest\Clientes\ClientesResource::class,
            'route_name' => 'cliente.rest.clientes',
            'route_identifier_name' => 'id',
            'collection_name' => 'data',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Cliente\V1\Rest\Clientes\ClientesEntity::class,
            'collection_class' => \Cliente\V1\Rest\Clientes\ClientesCollection::class,
            'service_name' => 'Clientes',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Cliente\\V1\\Rest\\Clientes\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'Cliente\\V1\\Rest\\Clientes\\Controller' => [
                0 => 'application/vnd.cliente.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Cliente\\V1\\Rest\\Clientes\\Controller' => [
                0 => 'application/vnd.cliente.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \Cliente\V1\Rest\Clientes\ClientesEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'cliente.rest.clientes',
                'route_identifier_name' => 'id',
                'hydrator' => \DoctrineModule\Stdlib\Hydrator\DoctrineObject::class,
            ],
            \Cliente\V1\Rest\Clientes\ClientesCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'cliente.rest.clientes',
                'route_identifier_name' => 'id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'Cliente\\V1\\Rest\\Clientes\\Controller' => [
            'input_filter' => 'Cliente\\V1\\Rest\\Clientes\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Cliente\\V1\\Rest\\Clientes\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'max' => '200',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'nome',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\EmailAddress::class,
                        'options' => [
                            'useMxCheck' => true,
                            'useDeepMxCheck' => true,
                            'useDomainCheck' => true,
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StripTags::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'email',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\NotEmpty::class,
                        'options' => [
                            'type' => 'all'
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\Boolean::class,
                        'options' => [
                            'type' => 'all'
                        ],
                    ],
                ],
                'name' => 'status',
            ],
        ],
    ],
];
