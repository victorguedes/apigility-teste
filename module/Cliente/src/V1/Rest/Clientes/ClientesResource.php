<?php
namespace Cliente\V1\Rest\Clientes;

use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class ClientesResource extends AbstractResourceListener implements ObjectManagerAwareInterface
{
    use ProvidesObjectManager;

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $hydrator = new DoctrineObject($this->getObjectManager());
        $object = $hydrator->hydrate($this->getInputFilter()->getValues(), new $this->entityClass());
        $this->getObjectManager()->persist($object);
        $this->getObjectManager()->flush();

        return $object;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        $object = $this->getObjectManager()->find($this->entityClass, $id);
        $this->getObjectManager()->remove($object);
        $this->getObjectManager()->flush();

        return true;
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->getObjectManager()->find($this->entityClass, $id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $repository = $this->getObjectManager()->getRepository($this->entityClass);
        return $repository->findAll();
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        $hydrator = new DoctrineObject($this->getObjectManager());
        $object = $this->getObjectManager()->find($this->entityClass, $id);
        $object = $hydrator->hydrate($this->getInputFilter()->getValues(), $object);
        $this->getObjectManager()->persist($object);
        $this->getObjectManager()->flush();

        return $object;
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $hydrator = new DoctrineObject($this->getObjectManager());
        $object = $this->getObjectManager()->find($this->entityClass, $id);
        $object = $hydrator->hydrate($this->getInputFilter()->getValues(), $object);
        $this->getObjectManager()->persist($object);
        $this->getObjectManager()->flush();

        return $object;
    }
}
