<?php
namespace Cliente\V1\Rest\Clientes;

class ClientesResourceFactory
{
    public function __invoke($services)
    {
        $resource = new ClientesResource();

        $objectManager = $services->get(\Doctrine\ORM\EntityManager::class);
        $resource->setObjectManager($objectManager);

        return $resource;
    }
}
